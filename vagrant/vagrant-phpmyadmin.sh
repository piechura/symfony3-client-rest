#!/bin/bash

#starting in /vagrant/vagrant
echo 'phpmyadmin phpmyadmin/dbconfig-install boolean true' | debconf-set-selections
echo 'phpmyadmin phpmyadmin/app-password-confirm password developer' | debconf-set-selections
echo 'phpmyadmin phpmyadmin/mysql/admin-pass password developer' | debconf-set-selections
echo 'phpmyadmin phpmyadmin/mysql/app-pass password developer' | debconf-set-selections
echo 'phpmyadmin phpmyadmin/reconfigure-webserver multiselect apache2' | debconf-set-selections

apt-get install -y phpmyadmin
