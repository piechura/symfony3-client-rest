#!/bin/bash

apt-get update
apt-get -y dist-upgrade
apt-get install -y aria2

mkdir /tmp/mysql
cd /tmp/mysql

aria2c --max-connection-per-server=5 --split=5 http://dev2.tvt.biz/cdn/mysql-server_5.6.34-1ubuntu14.04_i386.deb-bundle.tar

tar xvf mysql-server_5.6.34-1ubuntu14.04_i386.deb-bundle.tar


#echo mysql-server-5.5 mysql-server/root_password password PASSWORD | debconf-set-selections
#echo mysql-server-5.5 mysql-server/root_password_again password PASSWORD | debconf-set-selections

#echo mysql-server-5.6 mysql-server/root_password password developer | debconf-set-selections
#echo mysql-server-5.6 mysql-server/root_password_again password developer | debconf-set-selections

echo mysql-community-server mysql-community-server/root-pass password developer | debconf-set-selections
echo mysql-community-server mysql-community-server/re-root-pass password developer | debconf-set-selections
echo mysql-community-server mysql-community-server/remove-data-dir boolean true | debconf-set-selections
echo mysql-community-server mysql-community-server/data-dir note seen | debconf-set-selections

cd /tmp/mysql

dpkg -i *.deb

apt-get install -y -f

cd /vagrant/vagrant
rm -rf /tmp/mysql


#apt-get install -y mysql-common mysql-server mysql-client

sudo apt-get install -y python-software-properties
sudo add-apt-repository -y ppa:ondrej/php
sudo apt-get update -y
apt-cache pkgnames | grep php7.1

apt-get install -y libapache2-mod-php7.1 \
	php-common \
	php-xdebug \
	php7.1-bcmath \
	php7.1-bz2 \
	php7.1-cli \
	php7.1-common \
	php7.1-curl \
	php7.1-gd \
	php7.1-intl \
	php7.1-json \
	php7.1-mbstring \
	php7.1-mcrypt \
	php7.1-mysql \
	php7.1-opcache \
	php7.1-pgsql \
	php7.1-readline \
	php7.1-sqlite3 \
	php7.1-xml \
	php7.1-zip \
	php-apcu \
	vim \
	apache2

a2enmod vhost_alias
a2enmod rewrite


cd /vagrant/vagrant

cp config/root/.my.cnf						/root/.my.cnf
cp config/root/.my.cnf						/home/ubuntu/.my.cnf
chown ubuntu:ubuntu						/home/ubuntu/.my.cnf
cp config/etc/mysql/conf.d/custom.cnf				/etc/mysql/conf.d/custom.cnf
cp config/etc/php/7.1/mods-available/xdebug.ini 		/etc/php/7.1/mods-available/xdebug.ini
cp config/etc/apache2/conf-available/mass-virtual-hosting.conf 	/etc/apache2/conf-available/mass-virtual-hosting.conf

if [ -f /etc/apache2/sites-available/001-localhost.conf ]; then
    rm -f /etc/apache2/sites-available/001-localhost.conf
fi
cp config/etc/apache2/sites-available/001-localhost.conf        /etc/apache2/sites-available/001-localhost.conf
#ln -s /etc/apache2/conf-available/mass-virtual-hosting.conf 	/etc/apache2/conf-enabled/mass-virtual-hosting.conf

if [ -f /etc/apache2/sites-available/001-localhost.conf ]; then
    rm -f /etc/apache2/sites-enabled/001-localhost.conf
fi
ln -s /etc/apache2/sites-available/001-localhost.conf           /etc/apache2/sites-enabled/001-localhost.conf



sed -i "s~^error_reporting~; error_reporting~g" /etc/php/7.1/apache2/php.ini
echo  "error_reporting = E_ALL" >> /etc/php/7.1/apache2/php.ini

sed -i "s~^error_log~; error_log~g" /etc/php/7.1/apache2/php.ini
echo  "error_log = syslog" >> /etc/php/7.1/apache2/php.ini

service apache2 stop
sed -i "s~www-data~ubuntu~g" /etc/apache2/envvars

service apache2 start

service mysql restart

mysql  <<EOF
GRANT USAGE ON *.* TO 'developer'@'localhost';
DROP USER 'developer'@'localhost';

GRANT USAGE ON *.* TO 'developer'@'%';
DROP USER 'developer'@'%';

CREATE USER 'developer'@'localhost' IDENTIFIED BY 'developer';
CREATE USER 'developer'@'%' IDENTIFIED BY 'developer';

GRANT ALL PRIVILEGES ON * . * to 'developer'@'localhost';
GRANT ALL PRIVILEGES ON * . * to 'developer'@'%';

FLUSH PRIVILEGES;

EOF

#fix broken
apt-get -f install

apt-get install -y composer
cd /vagrant
su ubuntu -c 'composer install --no-interaction'

/vagrant/bin/console doctrine:database:create --if-not-exists
/vagrant/bin/console doctrine:schema:update --force