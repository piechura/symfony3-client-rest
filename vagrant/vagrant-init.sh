#!/bin/bash

#
#Install environment
#

{
    cd /vagrant/vagrant
    ./vagrant-lamp.sh
} || {
    echo -e "Something went wrong during install environment"
}


#
#Install dependencies
#

{
    echo -e "\e[1;36mInstall dependencies\e[0m"
    cd /vagrant/vagrant
    ./vagrant-composer.sh
} || {
    echo -e "Something went wrong during install composer"
}


#
# create or update db
#

{
    echo -e "\e[1;36mCreate or update db based on fixtures\e[0m"
    cd /vagrant/vagrant
    su ubuntu -c './symfony-database.sh'
} || {
    echo -e "Something went wrong during install database"
}



#
#Extra
#
{
    cd /vagrant/vagrant
    ./vagrant-phpmyadmin.sh
} || {
    echo -e "\e[1;31mSomething went wrong during install phpmyadmin\e[0m"
}

echo -e "Vagrant scripts finished"